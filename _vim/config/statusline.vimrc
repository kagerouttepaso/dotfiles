"-------------------------------------------------------------------------------
" ステータスライン StatusLine
"-------------------------------------------------------------------------------
set laststatus=2 " 常にステータスラインを表示
set ruler        " カーソルが何行目の何列目に置かれているかを表示する
set noshowmode   " vimデフォルトの -- insert -- みたいなものを表示しない

" vim-powerlineでフォントにパッチを当てないなら以下をコメントアウト
let g:Powerline_symbols = 'fancy'
set t_Co=256
